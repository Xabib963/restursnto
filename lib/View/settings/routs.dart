import 'package:get/get.dart';
import 'package:resturanto/app/Bindings/restListBindings.dart';
import 'package:resturanto/View/Screens/SplashScreen.dart';

import '../../app/Bindings/mapBindings.dart';
import '../../app/Bindings/splashBindings.dart';
import '../Screens/HomePage.dart';
import '../../../View/screens/Maps.dart';
import '../Screens/ResturantDeatiels.dart';
import '../Screens/ResturantsPage.dart';

appRoutes() => [
      GetPage(
        name: SplashScreen.routeName,
        page: () => SplashScreen(),
        binding: SplashBinding(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: HomePage.routeName,
        page: () => HomePage(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: ResturantsPage.routeName,
        page: () => ResturantsPage(),
        binding: HomeBinding(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: ResturantDeatiels.routeName,
        page: () => ResturantDeatiels(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: Maps.routeName,
        page: () => Maps(),
        binding: MapBindings(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
    ];
