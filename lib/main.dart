import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:resturanto/View/Screens/SplashScreen.dart';

import 'View/settings/Themes.dart';
import 'View/settings/routs.dart';
import 'app/Bindings/splashBindings.dart';
import 'View/Screens/HomePage.dart';
import 'View/Screens/Maps.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // {"amsterdam":'ChIJVXealLU_xkcRja_At0z9AGY','Rome':'ChIJu46S-ZZhLxMROG5lkwZ3D7k',"moscow":'ChIJybDUc_xKtUYRTM9XV8zWRD0',"berlin":'ChIJAVkDPzdOqEcRcDteW0YgIQQ'}
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: SplashScreen.routeName,
      title: 'Resturanto',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: OwnColors.secondColor),
        useMaterial3: true,
      ),
      initialBinding: SplashBinding(),
      getPages: appRoutes(),
    );
  }
}
