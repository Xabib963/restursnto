import 'package:get/get.dart';
import 'package:resturanto/View/Screens/HomePage.dart';
import 'package:resturanto/View/Screens/SplashScreen.dart';

class SlashController extends GetxController {
  @override
  void onInit() async {
    await Future.delayed(const Duration(seconds: 3));
    Get.offAllNamed(HomePage.routeName);
    super.onInit();
  }
}
