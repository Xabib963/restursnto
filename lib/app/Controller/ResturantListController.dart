import 'dart:convert';

import 'package:get/get.dart';
import 'package:resturanto/Data/model/Resturant.dart';
import 'package:http/http.dart' as http;

import '../../Data/model/City.dart';

class ResturantListController extends GetxController {
  //here should be the endpoint and the api key from rapidapi (the fork and the spoon)
  Future<List<Resturant>> getResturant(String place) async {
    var response = await http.get(Uri.parse(''));
    print((json.decode(response.body))['data']);
    List<Resturant> resturant = (json.decode(response.body))['data']
        .map<Resturant>((ap) => Resturant.fromJson(ap))
        .toList();
    print(resturant);
    return resturant;
  }
}
