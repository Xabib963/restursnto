import 'package:get/get.dart';
import 'package:resturanto/app/Controller/MapController.dart';

class MapBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(MapController());
  }
}
